﻿namespace com.alphapotato.Gameplay
{
    using UnityEngine;
using System.Collections;
using com.faithstudio.Gameplay;

public class CrowdBehaviour : MonoBehaviour
{

    #region Public Variables

    public Transform    crowdMeshTransformReference;
    public Animator     crowdAnimatorReference;

    [Space(10.0f)]
    [Range(0f,1000f)]
    public float        reactionForceOnImpact;
    public ForceMode    forceModeOfReactionOnImpact;

    #endregion

    #region Private Variables

    private bool        m_IsAbleToRecieveImpact;

    //[HideInInspector]
    public Rigidbody    rigidbodyReference;
    //[HideInInspector]
    public Rigidbody[]  rigidbodyReferenceOfBones;

    #endregion

    #region Mono Behaviour

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Tag_Contester") {

            EnableRagdoll();
            ApplyForceOnImpact(Vector3.Normalize(transform.position - other.transform.position));
        }
    }

    // private void OnCollisionEnter(Collision collision)
    // {
    //     if (collision.gameObject.tag == "Tag_Contester") {

    //         EnableRagdoll();
    //         ApplyForceOnImpact(Vector3.Normalize(transform.position - collision.transform.position));
    //     }
    // }

    #endregion

    #region Configuretion

    public void EnableRagdoll() {

        rigidbodyReference.isKinematic      = false;
        rigidbodyReference.detectCollisions = false;
        
        crowdAnimatorReference.enabled = false;

        int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
        for (int i = 0; i < t_NumberOfRagdollBone; i++) {

            rigidbodyReferenceOfBones[i].isKinematic = false;
        }
    }

    public void DisableRagdoll()
    {   
        int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
        for (int i = 0; i < t_NumberOfRagdollBone; i++)
        {

            rigidbodyReferenceOfBones[i].isKinematic = true;
        }

        crowdAnimatorReference.enabled = true;

        if (rigidbodyReference == null)
            Debug.Log("Ami ### : " + gameObject.name);

        rigidbodyReference.isKinematic      = true;
        rigidbodyReference.detectCollisions = true;
    }

    private void OnForceImpact(){

            CoinEarningController.Instance.AddMultipleCoinToUser(
                CrowdController.Instance.GetNumberOfCoinToBeRewardedForHittingCrowd(),
                crowdMeshTransformReference.position + Vector3.up * 3,
                Vector3.zero,
                Vector3.zero,
                CrowdController.Instance.GetCrowdCoinEarnMultiplier(),
                0.05f
            );
            
            //Camera Transation Start

            TimeController.Instance.DoSlowMotion(
                true,
                1.5f,
                0.1f,
                CrowdController.Instance.curveForSlowMotionOnHit,
                delegate{

                    TimeController.Instance.DoFastMotion(
                        true,
                        1.5f,
                        2,
                        CrowdController.Instance.curveForRestoreMotionOnHit,
                        delegate{

                            TimeController.Instance.RestoreToInitialTimeScale();
                        }
                    );

                    //Camera Transation End
                }
            );
    }

    private IEnumerator ControllerForCoinEarning(int t_NumberOfCoinToBeAdded){

        float t_CycleLength = 0.5f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);

        while(t_NumberOfCoinToBeAdded > 0){

            CoinEarningController.Instance.AddCoinToUser(
                crowdMeshTransformReference.position + Vector3.up * 3,
                Vector3.zero,
                Vector3.zero,
                CrowdController.Instance.GetCrowdCoinEarnMultiplier(),
                true
            );
            t_NumberOfCoinToBeAdded--;
            yield return t_CycleLength;
        }

        StopCoroutine(ControllerForCoinEarning(0));
    }

    #endregion

    #region Public Callback

#if UNITY_EDITOR

    public void ConfigRagdoll() {

        rigidbodyReferenceOfBones = crowdMeshTransformReference.GetComponentsInChildren<Rigidbody>();

        Rigidbody t_RigidbodyReference = gameObject.GetComponent<Rigidbody>();
        if (t_RigidbodyReference == null)
        {

            t_RigidbodyReference = gameObject.AddComponent<Rigidbody>();
        }

        t_RigidbodyReference.useGravity = false;
        t_RigidbodyReference.isKinematic = true;

        rigidbodyReference = t_RigidbodyReference;

        CapsuleCollider t_CapsulColliderReference = gameObject.GetComponent<CapsuleCollider>();
        if (t_CapsulColliderReference == null)
        {

            t_CapsulColliderReference = gameObject.AddComponent<CapsuleCollider>();
        }

        t_CapsulColliderReference.center = Vector3.up * 0.9f;
        t_CapsulColliderReference.height = 1.8f;
        t_CapsulColliderReference.isTrigger = false;
    }

#endif

    public void PreProcess(int t_NumberOfIdleAnimationClip = 0) {

        DisableRagdoll();

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        PlayRandomIdleAnimation(t_NumberOfIdleAnimationClip);

        m_IsAbleToRecieveImpact = true;
    }

    public void PostProcess() {

        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);

        m_IsAbleToRecieveImpact = false;

    }

    public void ApplyForceOnImpact(Vector3 t_ForceDirection = new Vector3(), float t_Force = 0) {

        if (m_IsAbleToRecieveImpact) {

            //if(Random.Range(0f,1f) <= 0.5f)
            //    GameplayController.Instance.ShowSadExpression(transform);

            Vector3 t_AbsoluteForce = (t_ForceDirection == Vector3.zero ? Random.onUnitSphere : t_ForceDirection);
            t_AbsoluteForce.y = Mathf.Abs(t_AbsoluteForce.y <= 0.25f ? 0.25f : t_AbsoluteForce.y);
            t_AbsoluteForce  *= (t_Force == 0 ? reactionForceOnImpact : t_Force);
            Debug.Log("ForceDirection : " + t_ForceDirection + ", Force : " + t_Force + ", AbsoluteForce : " + t_AbsoluteForce);

            int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
            for (int i = 0; i < t_NumberOfRagdollBone; i++)
            {
                rigidbodyReferenceOfBones[i].velocity = Vector3.zero;

                rigidbodyReferenceOfBones[i].AddForce(
                        t_AbsoluteForce,
                        forceModeOfReactionOnImpact
                    );

                rigidbodyReferenceOfBones[i].AddTorque(
                        t_AbsoluteForce * 3.0f,
                        forceModeOfReactionOnImpact
                    );
            }

            OnForceImpact();

            m_IsAbleToRecieveImpact = false;

        }
    }

    public void PlayRandomIdleAnimation(int t_NumberOfAnimationClip = 0) {

        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("IDLE" + t_AnimationIndex);
    }

    public void PlayRandomCheeringAnimation(int t_NumberOfAnimationClip = 0)
    {

        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("CHEER" + t_AnimationIndex);
    }

    public void PlayRandomSadAnimation(int t_NumberOfAnimationClip = 0)
    {
        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("SAD" + t_AnimationIndex);
    }

    public Transform GetCrowdMeshTransformReference() {

        return crowdMeshTransformReference;
    }

    #endregion
}

}
