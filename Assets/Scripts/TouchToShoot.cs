using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using com.alphapotato.Gameplay;

public class TouchToShoot : MonoBehaviour
{
    [Range(0f,10000f)]
    public float hitForce;

    public int totalCounter;
    public UnityEvent OnNPCFinishEvent;
    public UnityEvent OnLevelCompleteEvent;

    [Space(20)]
    public AnimationCurve curveForSlowMotionOnHit;


    public Collider hitCollider;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Gameplay.Instance.isGameRunning)
        {
            if (Input.GetMouseButtonUp(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    
                    var rig = hitInfo.collider;


                    if (rig.CompareTag("NPC"))
                    {
                        float t_Distance = Vector3.Distance(rig.transform.position, PlayerController.Instance.transform.position);

                        if (t_Distance < 2f)
                        {
                            //Perfect
                            StartCoroutine(PerfectRoutine(hitInfo));
                        }
                        else if(t_Distance>2f && t_Distance < 3f)
                        {
                            //Goood
                            StartCoroutine(GoodRoutine(hitInfo));
                        }
                        else if(t_Distance>3f && t_Distance<4)
                        {
                            // Bad
                            StartCoroutine(BadRoutine(hitInfo));
                        }
                    }
                }
            }
        }
    }
    IEnumerator PerfectRoutine(RaycastHit hitInfo)
    {
        hitCollider = hitInfo.collider;

        yield return new WaitForEndOfFrame();
        NPCMoveTowards npc = hitCollider.transform.root.GetComponent<NPCMoveTowards>();
        npc.FindSelectedPartsOfBody(hitCollider.transform.name);
        
        NPCBehaviour.Instance.ActivateNextNPC(npc.agentID, npc.targetPosID);
        UIManager.Instance.SetHintsText("Perfect");
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().DisableNPC();
        PlayerController.Instance.PlayPlayerReact();

        //hitCollider.transform.GetComponent<Rigidbody>().AddForceAtPosition(PlayerController.Instance.GetPlayerForward() * hitForce, hitCollider.transform.localPosition, ForceMode.Impulse);

        //yield return new WaitForSeconds(0.15f);
        //TimeController.Instance.DoSlowMotion();
       // yield return new WaitForSeconds(0.7f);
        //TimeController.Instance.RestoreToInitialTimeScale();
    }

    public void PerfectHitEffect()
    {
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().npcAnimator.enabled = false;
        hitCollider.transform.GetComponent<Rigidbody>().AddForceAtPosition(PlayerController.Instance.GetPlayerForward() * hitForce, hitCollider.transform.localPosition, ForceMode.Impulse);
        
        TimeController.Instance.DoSlowMotion(true,1f,0.2f,curveForSlowMotionOnHit,delegate
        {
            TimeController.Instance.RestoreToInitialTimeScale();
        });
    }


    IEnumerator GoodRoutine(RaycastHit hitInfo)
    {
        hitCollider = hitInfo.collider;

        yield return new WaitForEndOfFrame();
        NPCMoveTowards npc = hitCollider.transform.root.GetComponent<NPCMoveTowards>();
        npc.FindSelectedPartsOfBody(hitCollider.transform.name);
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().npcAnimator.enabled = false;
        hitCollider.transform.GetComponent<Rigidbody>().AddForceAtPosition(PlayerController.Instance.GetPlayerForward() * hitForce, hitCollider.transform.localPosition, ForceMode.Impulse);
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().DisableNPC();
        NPCBehaviour.Instance.ActivateNextNPC(npc.agentID, npc.targetPosID);
        UIManager.Instance.SetHintsText("Good");
        PlayerController.Instance.PlayPlayerReact();
    }
    IEnumerator BadRoutine(RaycastHit hitInfo)
    {
        hitCollider = hitInfo.collider;

        yield return new WaitForEndOfFrame();
        NPCMoveTowards npc = hitCollider.transform.root.GetComponent<NPCMoveTowards>();
        npc.FindSelectedPartsOfBody(hitCollider.transform.name);
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().npcAnimator.enabled = false;
        hitCollider.transform.GetComponent<Rigidbody>().AddForceAtPosition(PlayerController.Instance.GetPlayerForward() * hitForce, hitCollider.transform.localPosition, ForceMode.Impulse);
        hitCollider.transform.root.GetComponent<NPCMoveTowards>().DisableNPC();
        NPCBehaviour.Instance.ActivateNextNPC(npc.agentID, npc.targetPosID);
        UIManager.Instance.SetHintsText("Bad Hit, Cooldown for 3 seconds");
        Gameplay.Instance.CoolDownHit();
        PlayerController.Instance.PlayPlayerReact();
    }
}