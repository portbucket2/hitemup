using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    

    public GameObject joystickObject;
    public bool isGameRunning;


   

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
       
        
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }
    IEnumerator StartGameRoutine()
    {
        
        joystickObject.SetActive(true);
        yield return new WaitForEndOfFrame();
        
        isGameRunning = true;

        NPCBehaviour.Instance.ActivateNPCPhaseOne();

        //InvokeRepeating("SpawnCharReal", 2, 12);
        //InvokeRepeating("SpawnCharRealRight", 5, 15);
    }

    

    public void ChangeDirection()
    {
        StartCoroutine(ChangeDirectionRoutine());
    }

    private IEnumerator ChangeDirectionRoutine()
    {
        yield return new WaitForEndOfFrame();
        PlayerController.Instance.ChangePlayerDirection();
        yield return new  WaitForSeconds(1f);
       
    }

    public void LevelComplete()
    {
        joystickObject.SetActive(false);
        UIManager.Instance.ShowLevelComplete();
    }



    //public void SpawnChar()
    //{
    //    for (int i = 0; i < spawnObjectListNPC.Count; i++)
    //    {
    //        if (!spawnObjectListNPC[i].GetComponent<NPCMoveTowards>().isAlive)
    //        {
    //            spawnObjectListNPC[i].SetActive(true);
    //            spawnObjectListNPC[i].GetComponent<NPCMoveTowards>().ActivateNPC();
    //            return;
    //        }
    //    }
       
    //}
    //public void SpawnCharReal()
    //{
    //    for (int i = 0; i < spawnObjectListReal.Count; i++)
    //    {
    //        if (!spawnObjectListReal[i].GetComponent<NPCMoveTowards>().isAlive)
    //        {
    //            spawnObjectListReal[i].SetActive(true);
    //            spawnObjectListReal[i].GetComponent<NPCMoveTowards>().ActivateNPC();
    //            return;
    //        }
    //    }

    //}
    //public void SpawnCharRealRight()
    //{
    //    for (int i = 0; i < spawnObjectListRight.Count; i++)
    //    {
    //        if (!spawnObjectListRight[i].GetComponent<NPCMoveTowards>().isAlive)
    //        {
    //            spawnObjectListRight[i].SetActive(true);
    //            spawnObjectListRight[i].GetComponent<NPCMoveTowards>().ActivateNPC();
    //            return;
    //        }
    //    }

    //}

    public void CoolDownHit()
    {
        StartCoroutine(WaitAndOff());
    }
    private IEnumerator WaitAndOff()
    {
        isGameRunning = false;
        joystickObject.SetActive(false);
        yield return new WaitForSeconds(3f);
        joystickObject.SetActive(true);
        isGameRunning = true;
    }
}
