using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehaviour : MonoBehaviour
{
    public static NPCBehaviour Instance;

    
    public List<NPCMoveTowards> frontSideNPCList;
    public List<NPCMoveTowards> rightSideNPCList;
    public List<NPCMoveTowards> leftSideNPCList;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void ActivateNPCPhaseOne()
    {
        for (int i = 0; i < frontSideNPCList.Count; i++)
        {
            frontSideNPCList[i].ActivateNPC();
        }
        for (int i = 0; i < rightSideNPCList.Count; i++)
        {
            rightSideNPCList[i].ActivateNPC();
        }
        for (int i = 0; i < leftSideNPCList.Count; i++)
        {
            leftSideNPCList[i].ActivateNPC();
        }
    }

    public void SetAgentID(int agentID,int posID)
    {
        switch (posID)
        {
            case 0:
                for (int i = 0; i < frontSideNPCList.Count; i++)
                {
                    if (frontSideNPCList[i].agentID != agentID)
                    {
                        frontSideNPCList[i].MoveStopAndWait();
                    }
                }
                break;
            case 1:
                for (int i = 0; i < rightSideNPCList.Count; i++)
                {
                    if (rightSideNPCList[i].agentID != agentID)
                    {
                        rightSideNPCList[i].MoveStopAndWait();
                    }
                }
                break;

            case 2:
                for (int i = 0; i < leftSideNPCList.Count; i++)
                {
                    if (leftSideNPCList[i].agentID != agentID)
                    {
                        leftSideNPCList[i].MoveStopAndWait();
                    }
                }
                break;
            default:
                break;
        }
        
    }

    public void ActivateNextNPC(int agentID, int targetPosID)
    {
        switch (targetPosID)
        {
            case 0:
                for (int i = 0; i < frontSideNPCList.Count; i++)
                {
                    if (frontSideNPCList[i].agentID != agentID)
                    {
                        frontSideNPCList[i].MoveAgain();
                    }
                }
                break;
            case 1:
                for (int i = 0; i < rightSideNPCList.Count; i++)
                {
                    if (rightSideNPCList[i].agentID != agentID)
                    {
                        rightSideNPCList[i].MoveAgain();
                    }
                }
                break;
            case 2:
                for (int i = 0; i < leftSideNPCList.Count; i++)
                {
                    if (leftSideNPCList[i].agentID != agentID)
                    {
                        leftSideNPCList[i].MoveAgain();
                    }
                }
                break;
            default:
                break;
        }
    }
}
