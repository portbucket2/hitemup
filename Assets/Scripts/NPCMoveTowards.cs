using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RootMotion.Dynamics;
using UnityEngine.AI;


public class NPCMoveTowards : MonoBehaviour
{
    public bool isReachedToPlayer;
    public int targetPosID;
    public int agentID;

    private int WALK = Animator.StringToHash("walk");
    private int IDLE = Animator.StringToHash("idle");
    private int RUN = Animator.StringToHash("run");
    private int PUNCH = Animator.StringToHash("punch");
    private int kick = Animator.StringToHash("kick");

    [Range(0f, 20f)] public float movingSpeed;

    public Transform target;
    public Animator npcAnimator;
    public bool isMove;
    public bool isAlive;
    public Vector3 initPos;

   

    [Space(10)]
    public NavMeshAgent agent;

    [Space(10)]
    public UnityEvent OnDiedEvent;


    [Space(10)]
    public PuppetMaster puppetMaster;
    public Muscle[] muscleArray;



    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }


    private void Start()
    {
        muscleArray = puppetMaster.muscles;
        initPos = transform.position;
    }

    public void MoveStopAndWait()
    {
        Debug.LogError("ITS HERE");
        npcAnimator.SetTrigger(IDLE);
        if (agent.isActiveAndEnabled)
        {
            agent.isStopped = true;
        }
    }

    public void MoveAgain()
    {
        npcAnimator.SetTrigger(WALK);
        if (agent.isActiveAndEnabled)
        {
            agent.isStopped = false;
        }
    }


    public void FindSelectedPartsOfBody(string name)
    {
        for (int i = 0; i < muscleArray.Length; i++)
        {
            if (string.Compare(muscleArray[i].name, name) == 0)
            {
                Debug.Log(name);
                StartCoroutine(EffectRoutine(muscleArray[i]));
            }
        }
    }

   

    private IEnumerator EffectRoutine(Muscle muscle)
    {
        muscle.props.pinWeight = 0.1f;
        muscle.props.muscleDamper = 0.1f;

        muscle.props.muscleWeight = 0f;

        while (puppetMaster.pinWeight >0)
        {
            yield return new WaitForEndOfFrame();
            puppetMaster.pinWeight -= Time.deltaTime*10;
        }

        muscle.props.muscleWeight = 1f;
    }

    public void ResetNPC()
    {
        puppetMaster.pinWeight = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMove)
        {
            if (!isReachedToPlayer)
            {
                agent.SetDestination(target.position);
            }

            if (Vector3.Distance(transform.position, target.position) < 0.1f && !isReachedToPlayer)
            {
                NPCBehaviour.Instance.SetAgentID(agentID, targetPosID);
                isReachedToPlayer = true;
                npcAnimator.SetTrigger(PUNCH);
            
            }
            
        }
    }

    public void DisableCollider()
    {
        GetComponent<CapsuleCollider>().enabled = false;
    }
    public void DisableAnimation()
    {
        npcAnimator.enabled = false;
    }
    public void DisableNPC()
    {
        isMove = false;
       
        StartCoroutine(WaitAndDeactivate());
    }
    public void ActivateNPC()
    {
        isMove = true;
        isAlive = true;
        npcAnimator.SetTrigger("walk");
    }

    IEnumerator WaitAndDeactivate()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
        isAlive = false;
        transform.position = initPos;
        ResetNPC();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("BURST OUt " + other.transform.name);
        if (other.transform.CompareTag("NPC"))
        {
            Debug.Log("BURST IN");
            other.transform.GetComponent<NPCMoveTowards>().DisableCollider();
            other.transform.GetComponent<NPCMoveTowards>().DisableAnimation();
            other.transform.GetComponent<NPCMoveTowards>().DisableNPC();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
       
    }
}
