using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public Transform playerBody;

    public VariableJoystick variableJoystick;

    private float xRotation;
    private float yRotation;

    

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = variableJoystick.Horizontal * mouseSensitivity * Time.deltaTime ;
        float mouseY = variableJoystick.Vertical * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        yRotation -= mouseX;

        xRotation = Mathf.Clamp(xRotation, -10f, 20f);
        
        
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        

        if ((yRotation < 60 && yRotation >0) || (yRotation<0 && yRotation>-60))
        {
            playerBody.Rotate(Vector3.up * mouseX);
        }
        
    }

    public Vector3 GetCameraRotation()
    {
        return transform.localRotation.eulerAngles;
    }
}
