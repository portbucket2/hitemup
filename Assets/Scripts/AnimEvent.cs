using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEvent : MonoBehaviour
{
    public UnityEvent OnPunchEvent;
    public UnityEvent OnKickEvent;


    public void PunchEvent()
    {
        OnPunchEvent.Invoke(); 
    }

    public void KickEvent()
    {
        OnKickEvent.Invoke();
    }
}
