using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public Animator playerAnimator;
    public MouseLook mouseLook;


    private int PUNCH = Animator.StringToHash("punch");
    private int KICK = Animator.StringToHash("kick");

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }


    public void PlayPlayerReact()
    {
        if (mouseLook.GetCameraRotation().x >= 15f)
        {
            PlayKick();
        }
        else
        {
            PlayPunch();
        }
    }
    public void PlayPunch()
    {
        playerAnimator.SetTrigger(PUNCH);
    }

    public void PlayKick()
    {
        playerAnimator.SetTrigger(KICK);
    }


    public void ChangePlayerDirection()
    {
        //transform.Rotate(Vector3.up, 180);
        StartCoroutine(YAxisRotationRoutine(180,delegate {

            Debug.Log("Start 2nd Hre");
	    }));
    }

    public IEnumerator YAxisRotationRoutine(float t_DesPos, UnityAction t_Action = null)
    {
        float t_Progression = 0f;
        float t_Duration = 2f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 0;
        float t_DestValue = t_DesPos;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            //transform.Rotate(Vector3.up, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            transform.rotation = Quaternion.Euler(0, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), 0);
            

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(YAxisRotationRoutine(0, null));
    }

    public Vector3 GetPlayerForward()
    {
        return transform.forward;
    }
}
